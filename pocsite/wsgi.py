import os
import sys

from django.core.wsgi import get_wsgi_application
from django.conf import settings
from django.contrib.staticfiles.handlers import StaticFilesHandler


print('[INFO] Using WSGI', file=sys.stderr)

os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'pocsite.settings.image')

if settings.STATIC_WSGI:
    print('[INFO] Using StaticFilesHandler')
    application = StaticFilesHandler(get_wsgi_application())
else:
    application = get_wsgi_application()
