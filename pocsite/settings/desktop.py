from .common import *
print('[INFO] Importing desktop settings using sqlite')

SECRET_KEY = 'django-insecure-%q#z!x!5!44(t2h1)o55!&igl$-xxpjugh+$lt5t5n@7ppkcm3'

DEBUG = True
STATIC_WSGI = False

ALLOWED_HOSTS = []

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': 'db.sqlite3',
    }
}
